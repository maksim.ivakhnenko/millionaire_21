from pathlib import Path


WORK_DIR: Path = Path(__file__).resolve().parent.parent
DATA_DIR: Path = WORK_DIR / 'data'
