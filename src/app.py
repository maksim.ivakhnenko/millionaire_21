from inspect import currentframe
import pickle
from pathlib import Path
from re import A
import typing
import nmslib
import numpy as np
from loguru import logger
from nmslib.dist import FloatIndex
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer

from src.constants import DATA_DIR
from src.neighbours import get_answers
from src.schemas import HelpOptions, PredictInputModel, PredictAnswerModel, ResultQuestionInputModel, EndGameOptions
from src.tf_idf import load_tf_idf
from decision import decision

from flask import Flask
from flask import request

app = Flask(__name__)

logger.debug(f"Load Tf-Idf")
tf_idf_path: Path = DATA_DIR / "tf_idf.pickle"
tf_idf: TfidfVectorizer = load_tf_idf(path=tf_idf_path)

logger.debug(f"Load PCA")
pca_path: Path = DATA_DIR / "PCA.pickle"
with pca_path.open("rb") as file:
    pca: TruncatedSVD = pickle.load(file)


logger.debug(f"Load index")
index_path: Path = DATA_DIR / "nn_index.bin"
index: FloatIndex = nmslib.init(method='hnsw', space='cosinesimil', data_type=nmslib.DataType.DENSE_VECTOR)
index.loadIndex(str(index_path))

logger.debug(f"Service ready to work!")


money = [(100, False),
    (200, False),
    (300, False),
    (500, False),
    (1000, True),
    (2000, False),
    (4000, False),
    (8000, False),
    (16000, False),
    (32000, True),
    (64000, False),
    (125000, False),
    (250000, False),
    (500000, False),
    (1000000, True)]

num_a: typing.Optional[int] = None


@app.route("/predict", methods=['POST'])
def predict():
    global num_a
    data = request.form.to_dict(flat=False)
    data = {key: value[0] if len(value) == 1 else value for key, value in data.items()}
    input_data: PredictInputModel = PredictInputModel(**data)

    try:
        print(input_data.question)
        texts = [
            "" if answer is None else f"{input_data.question} {answer}"
            for answer in input_data.answers_list
        ]
        vectors: np.ndarray = pca.transform(tf_idf.transform(raw_documents=texts))

        answers_score: typing.List[float] = get_answers(index=index, vectors=vectors)
        cash_list = [x[0] for x in money]
        current_bank = cash_list[cash_list.index(input_data.question_money) - 1]
        available_hints = [v.value for v in input_data.available_help]
        dec, score = decision(
            answer_scores=answers_score, bank_money=current_bank, saved_money=input_data.saved_money,
            question_number=cash_list.index(input_data.question_money), available_hints=available_hints,
            money=money, num_a=num_a,
        )

        num_a = None
        if dec == 'take money':
            response: PredictAnswerModel = PredictAnswerModel()
            response.end_game = EndGameOptions.TAKE_MONEY
        elif dec == 'fifty fifty':
            response: PredictAnswerModel = PredictAnswerModel(help=HelpOptions.FIFTY_FIFTY)
        elif dec == 'new question':
            response: PredictAnswerModel = PredictAnswerModel(help=HelpOptions.NEW_QUESTION)
        elif dec == 'can mistake':
            response: PredictAnswerModel = PredictAnswerModel(answer=score, help=HelpOptions.CAN_MISTAKE)
            num_a = score
        elif dec == 'answer':
            response: PredictAnswerModel = PredictAnswerModel(answer=score)
    except Exception as e:
        logger.error(e)
        response: PredictAnswerModel = PredictAnswerModel(answer=1)

    return response.json()


@app.route("/result_question", methods=['POST'])
def result_question():
    data = request.form.to_dict(flat=False)
    data = {key: value[0] if len(value) == 1 else value for key, value in data.items()}
    input_data: ResultQuestionInputModel = ResultQuestionInputModel(**dict(data))

    return {'data': 'ok'}


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=12300)
