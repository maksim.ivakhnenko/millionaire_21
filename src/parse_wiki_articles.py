import typing
from pathlib import Path
from bs4 import BeautifulSoup


def wiki_articles_generator(wiki_texts_dir: Path) -> typing.Generator[str, None, None]:
    """Generate articles from dir with wiki texts.

    Args:
        wiki_texts_dir: path to text dir with wiki files after wikiextractor.

    Generate:
        article: string wiki article.
    """
    for sub_dir in wiki_texts_dir.glob("*"):
        sub_dir: Path = wiki_texts_dir / sub_dir
        for filename in sub_dir.glob("*"):
            filename: Path = sub_dir / filename
            with filename.open('r') as file:
                soup: BeautifulSoup = BeautifulSoup(file.read())
                for doc_tag in soup.find_all('doc'):
                    article: str = doc_tag.get_text()
                    yield article
